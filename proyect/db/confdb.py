import pyodbc
import sys
import utilities.utilities as utilities

class SqlConf:

    def configure(__self):        
        driver_list = pyodbc.drivers()
        try:
            utilities_model = utilities.utilities()
            db_config = utilities_model.DB_CONFIG            
            driver = '{'+driver_list[0]+'}'
            conn_str = f'DRIVER={driver};SERVER={db_config.get("SERVER")};DATABASE={db_config.get("DATABASE")};UID={db_config.get("UID")};PWD={db_config.get("PASS")};TrustServerCertificate=yes'
            conn = pyodbc.connect(conn_str)
        except Exception as exception:
            print("Error al conectarse a la base de dato: " + str(exception))
            sys.exit()
        finally:
            return conn