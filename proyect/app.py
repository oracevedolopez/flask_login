from flask import Flask
from flask import request
import json
import models.Users as Users
import sys

app = Flask(__name__)

@app.route('/login', methods= ['POST'])
def login():
    data = ''
    if request.method == 'POST':
        data = request.get_json(silent=True)
    if request.method == 'GET':
        data = request.args
    #obtenemos la informacion
    users_model = Users.Users()
    users_model.username = data["usuario"]
    users_model.password = data["pass"]
    user_data = users_model.LoginUser()
    
    data_info = {}
    username = user_data[0][0]
    token = user_data[0][1]
    if(token == 1):
        data_info['estado'] = False
        data_info['descriptionRespuesta'] = user_data[0][0]
        data_info['token'] = ''
    else:
        data_info['estado'] = True
        data_info['descriptionRespuesta'] = ''
        data_info['token'] = user_data[0][1]
    data_json = json.dumps(data_info);    
    return(data_json)

if __name__ == "__main__":
    app.run(debug=True)