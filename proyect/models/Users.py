import db.confdb as confdb
import sys

class Users:
    def __init__(self):
        self.id = 0
        self.username = ""
        self.password = ""
        self.token = ""
    
    def LoginUser(self):
        script = f"EXEC usp_auth_login '{self.username}', '{self.password}'"
        try:
            sql_conf = confdb.SqlConf()
            conn = sql_conf.configure()
            cursor = conn.cursor()
            cursor.execute(script)
            data_result = cursor.fetchall()
            conn.commit()            
        except Exception as e:
            print("Ships error ")
            print(script)
            print(e)
        conn.close()
        return data_result 
        