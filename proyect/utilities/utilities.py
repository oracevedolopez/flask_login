import sys
class utilities:
    '''Variables estaticas'''
    DB_CONFIG = {
        'DRIVER_SQL':'SQL Server Native Client 11.0',
        'SERVER':'127.0.0.1',
        'DATABASE':'flaskTest',
        'UID':'apiTest',
        'PASS':'',
        'CHARSET':'utf8'
    }
    def ExtractXMLData(self,string_xml,initial_tag,final_tag):
        #realizamos la busqueda y extraemos el texto si se encuentra
        initial_tag_len = len(initial_tag)
        initial_tag_position = string_xml.find(initial_tag)
        initial_tag_position = int(initial_tag_position)
        final_tagposition = string_xml.find(final_tag)
        final_tagposition = int(final_tagposition)
        find_string = ""
        if initial_tag_position > 0 and final_tagposition > 0:
            find_string = string_xml[initial_tag_position+initial_tag_len : final_tagposition]
        #return_string = f"<root>{find_string}</root>"
        return find_string