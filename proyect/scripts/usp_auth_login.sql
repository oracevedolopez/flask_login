USE [flaskTest]
GO

/****** Object:  StoredProcedure [dbo].[usp_auth_login]    Script Date: 11/02/2023 10:42:38 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		oracevedo
-- Create date: 10/02/2023
-- Description:	valida usuario y contraseņa, tambien actualiza los token por expiracion
-- =============================================
CREATE PROCEDURE [dbo].[usp_auth_login] 
	-- Add the parameters for the stored procedure here
	@username nvarchar(15),
	@pass nvarchar(25)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT TOP 1 username FROM authUser WHERE username = @username AND password = @pass)
	BEGIN
	--evaluamos si el token esta vigente, de lo contrario lo actualzamos y retornamo sel nuevo token
		IF EXISTS(SELECT TOP 1 username FROM authUser WHERE username = @username AND password = @pass AND expirationDate >= GETDATE())
		BEGIN
			SELECT TOP 1 username, token FROM authUser WHERE username = @username AND password = @pass;
		END
		ELSE
		BEGIN
			--actualizamos el token
			DECLARE @BinaryData varbinary(max) , @CharacterData varchar(max) , @Length int = 200;
			SET @BinaryData=crypt_gen_random (@Length);
			SET @CharacterData=cast('' as xml).value('xs:base64Binary(sql:variable("@BinaryData"))', 'varchar(25)');
			UPDATE authUser SET token = @CharacterData, expirationDate = DATEADD(DAY, 10, GETDATE()) WHERE username = @username AND password = @pass;
			SELECT TOP 1 username, token FROM authUser WHERE username = @username AND password = @pass;
		END	
	END
	ELSE
	BEGIN
	SELECT 'User or password incorrect' as 'msg', 1 as 'error';
	END
END
GO


