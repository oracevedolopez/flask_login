USE [flaskTest]
GO

/****** Object:  Table [dbo].[authUser]    Script Date: 11/02/2023 10:42:13 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[authUser](
	[authUserId] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](15) NOT NULL,
	[password] [nvarchar](150) NOT NULL,
	[token] [nvarchar](150) NOT NULL,
	[expirationDate] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[authUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


