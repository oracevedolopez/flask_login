FROM ubuntu:22.04
WORKDIR /usr/src/app
RUN apt-get update
RUN apt-get install python3 -y \
    python3-pip \
    curl \
    dpkg \
    unixodbc \
    odbcinst
RUN apt-get update
RUN apt-get clean -y
COPY requirements.txt ./
RUN pip install -r requirements.txt
RUN mkdir -p /usr/src/app/flask
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/22.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update
RUN ACCEPT_EULA=Y apt-get install -y msodbcsql18
WORKDIR /usr/src/app/flask
COPY proyect/ ./
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
